Source: govendor
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-bowery-prompt-dev,
               golang-github-dchest-safefile-dev,
               golang-github-google-shlex-dev,
               golang-github-pkg-errors-dev,
               golang-golang-x-crypto-dev,
               golang-golang-x-tools-dev,
               golang-gopkg-yaml.v2-dev
Standards-Version: 4.0.0
Homepage: https://github.com/kardianos/govendor
Vcs-Browser: https://salsa.debian.org/go-team/packages/govendor
Vcs-Git: https://salsa.debian.org/go-team/packages/govendor.git
XS-Go-Import-Path: github.com/kardianos/govendor
Testsuite: autopkgtest-pkg-go

Package: govendor
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Go vendor tool that works with the standard vendor file
 govendor is a vendoring tool that fetches, annotates, and synchronizes
 dependencies for the Go programming language. It can be useful as a
 stand-alone tool even if it is not the primary vendoring tool. Inspection
 sub-commands such as "govendor list" and "govendor license" can be useful
 to find information on dependencies and their statuses. "govendor fetch"
 will update or add new dependencies directly from remotes. If you choose
 to not check in your dependency source code, use "govendor sync" to pull
 the correct revision into the vendor folder.
 .
 Frustrated by not being able to run "go test ./..." anymore? Run
 "govendor test +local" even if you do not use govendor elsewhere. Look
 into the different statuses that can be assigned to packages, they are
 useful. Still using godep or glock? Migrate them over including manifest
 file using "govendor migrate".
